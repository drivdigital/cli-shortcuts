<?php

require __DIR__.'/../laravel/vendor/autoload.php';

define('LARAVEL_START', microtime(true));

use Symfony\Component\Console\Input\ArrayInput;

function get_kernel() {
  static $app;
  static $kernel;
  if ( ! $app ) {
    $app = require_once __DIR__.'/bootstrap/app.php';
    $kernel = $app->make(Illuminate\Contracts\Console\Kernel::class);
  }
  return $kernel;
}

function call( $parameters ) {
  static $output;
  if ( ! $output ) {
    $output = new Symfony\Component\Console\Output\ConsoleOutput;
  }
  $kernel = get_kernel();
  $artisan = $kernel->handle( new ArrayInput( $parameters ), $output );
}

function validate_config( $config ) {
  // echo call( ['validate_config'] );
}
