<?php

use Illuminate\Foundation\Inspiring;

Artisan::command('build {project}', function ($project) {
    $this->info("Building {$project}!");
});