#!/usr/bin/php
<?php

$path = __DIR__.'/vendor/rupa/z';
shell_exec( 'mkdir -p '. dirname( $path ) );

if ( ! file_exists( $path ) ) {
  exec( "git clone https://github.com/rupa/z.git '$path'", $result, $return );

  if ( 0 != $return ) {
    die( "Could not clone rupa/z\n" );
  }
}
