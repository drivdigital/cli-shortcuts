<?php

namespace App\Scripts\Htaccess;

use CLI\Shortcuts\Script;

class Htaccess extends \CLI\Shortcuts\Script {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'htaccess';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a standard htaccess';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if (! $this->config_check()) {
            return;
        }

        $config = $this->get_config();

        // Get the project root
        $dir = $this->get_project_dir();
        if (! $dir) {
            $this->comment('It looks like this is not a web project');
            if (! $this->confirm('Do you want to continue?')) {
                return;
            }
        }
        if (is_dir("$dir/public")) {
            $dir .= '/public';
        }
        $path = "$dir/.htaccess";
        if (file_exists($path)) {
            $this->warn("Htaccess already exist @ '$path'");
            return;
        }
        touch($path);
        if (! is_writable($path)) {
            $this->error("Can't write to '$path'");
            return;
        }
        $content = <<<HTACCESS
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
</IfModule>
HTACCESS;
        file_put_contents($path, $content);
    }
}
