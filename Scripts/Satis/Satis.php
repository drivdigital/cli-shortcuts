<?php

namespace App\Scripts\Satis;

use CLI\Shortcuts\Script;

class Satis extends \CLI\Shortcuts\Script {

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'satis';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Publish any project to satis.driv.digital';

  public $wp_plugin_info = [];

  /**
   * Get config
   * @return array
   */
  public function get_config() {
    $filepath = base_path( '../config.json' );
    if ( ! file_exists( $filepath ) ) {
      $config = [];
    } else {
      $content = file_get_contents( $filepath );
      $config = json_decode( $content, 1 );
      if ( ! $config ) {
        $this->error( 'JSON-error in the config.json file');
        return [];
      }
    }
    return $config;
  }
  /**
   * Save config
   * @param  array   $config
   * @return boolean
   */
  public function save_config( $config ) {
    $filepath = base_path( '../config.json' );
    $content = json_encode( $config, JSON_PRETTY_PRINT );
    if ( ! $content ) {
      return false;
    }
    return file_put_contents( $filepath, $content );
  }

  /**
   * Config check
   * @return [type] [description]
   */
  public function config_check() {
    $config = $this->get_config();
    if ( empty( $config ) ) {
      return false;
    }

    if ( ! isset( $config['satis.driv.digital'] ) || ! isset( $config['satis.driv.digital']['username'] ) ) {
      $config['satis.driv.digital']['username'] = $this->ask( 'What\'s your username for satis.driv.digital? (you can change this later in cli-shortcuts/config.json)' );
      if ( ! preg_match( '/^[A-Za-z\-]+$/', $config['satis.driv.digital']['username'] ) ) {
        $this->error( 'Invalid username!' );
        return false;
      }
      $this->save_config( $config );
    }
    return true;
  }
  /**
   * Get File Data
   * @param  string $file
   * @return string
   */
  function getFileData($file)
  {
    // We don't need to write to the file, so just open for reading.
    $fp = fopen( $file, 'r' );
    // Pull only the first 8kiB of the file in.
    $file_data = fread( $fp, 8192 );
    // PHP will close file handle, but we are good citizens.
    fclose( $fp );
    // Make sure we catch CR-only line endings.
    $file_data = str_replace( "\r", "\n", $file_data );
    return $file_data;
  }

  /**
   * Get Plugin Info
   * @param  string     $file
   * @return array|null
   */
  public function getPluginInfo($file)
  {
    $content = $this->getFileData($file);
    $lines = explode("\n", $content);
    $info = [];
    foreach ($lines as $line) {
      if (preg_match('/Plugin URI\s*:\s*(\S+)\s*$/', $line, $matches)){
        $info['plugin uri'] = $matches[1];
      }
      if (preg_match('/Plugin Slug\s*:\s*(\S+)\s*$/', $line, $matches)){
        $info['plugin slug'] = $matches[1];
      }
      if (preg_match('/Version\s*:\s*(\S+)\s*$/', $line, $matches)){
        $info['version'] = $matches[1];
      }
      if (preg_match('/Description\s*:\s*(.*)$/', $line, $matches)){
        $info['description'] = $matches[1];
      }
    }
    if (empty($info)) {
      return null;
    }
    return $info;
  }

  /**
   * Get Plugin Info From Dir
   * @param  string     $dir
   * @return array|null
   */
  public function getPluginInfoFromDir($dir = '.')
  {
    $files = glob("$dir/*.php");
    $info = null;
    foreach ($files as $file) {
      $info = $this->getPluginInfo($file);
      if ($info) {
        break;
      }
    }
    return $info;
  }

  /**
   * Get Plugin Field
   * @param  string      $key
   * @return string|null
   */
  public function getPluginField($key)
  {
    if (null === $this->wp_plugin_info) {
      return null;
    }
    if (empty($this->wp_plugin_info)) {
      $this->wp_plugin_info = $this->getPluginInfoFromDir();
    }
    return $this->wp_plugin_info[$key] ?? null;
  }

  /**
   * Get WP Plugin Name
   * @return string
   */
  public function getWPPLuginName()
  {
    $url = $this->getPluginField('plugin uri');
    $slug = $this->getPluginField('plugin slug');
    if (!$url || !$slug) {
      return null;
    }
    if (! preg_match('/([^\.\/]+)\.[a-z]+\/?\s*$/', $url, $matches)) {
      return null;
    }
    $company = $matches[1];
    return "$company/$slug";
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {

    if ( ! $this->config_check() ) {
      return;
    }

    $config = $this->get_config();
    $username = $config['satis.driv.digital']['username'];

    $dir = getcwd();
    // $this->info( "$dir" );
    if ( ! file_exists( "$dir/composer.json" ) ) {
      touch("$dir/composer.json");
    }
    if ( ! file_exists( "$dir/composer.json" ) ) {
      $this->error("No composer.json found");
      return;
    }
    $content = file_get_contents( "$dir/composer.json" );
    $data = json_decode( $content, 1 );
    $updated = false;

    if ( is_dir( '.git' ) && `git status -s` ) {
      $this->error( "WARNING! There are changes not yet committed!" );
      $this->comment( trim( `git status -s` ) );
      if ( ! $this->confirm( 'Continue?' ) ) {
        return;
      }
      $this->error( "WARNING! You are responsible for any changes beyond this point!" );
    }

    if ( is_dir( '.git' ) && trim( `git rev-parse --abbrev-ref HEAD` ) !== 'master' ) {
      $this->info( `git rev-parse --abbrev-ref HEAD` );
      if ( ! $this->confirm( "You're not in the master branch. Are you sure you want to continue?", false ) ) {
        return;
      }
    }
    if ( ! isset( $data['version'] ) || ! $data['version'] ) {
      $default =  $this->getPluginField('version');
      $this->comment( "The version number is missing from composer.json" );
      $data['version'] = $this->ask( 'What version number would you like to use? (eg.: "1.4.4")', $default );
      $updated = true;
    }
    if ( ! isset( $data['name'] ) || ! $data['name'] ) {
      $default = $this->getWPPLuginName();
      $this->comment( "The name is missing from composer.json" );
      $data['name'] = $this->ask( 'What name would you like to use?', $default );
      $updated = true;
    }
    if ( ! preg_match('/^[a-z\d\-\_]+\/[a-z\d\-\_]+$/', $data['name']) ) {
      $this->error( 'Invalid name! The name must use the following format: company-name/package-name' );
      return;
    }
    if ( ! preg_match('/^\d+\.\d+\.\d+(\.\d+)?$/', $data['version']) ) {
      $this->error( 'Invalid version number! The version number must use the following format: #.#.#' );
      return;
    }
    if ( ! isset( $data['name'] ) ) {
      $this->error("Missing name in composer.json");
      return;
    }
    if ( ! isset( $data['version'] ) ) {
      $this->error("Missing version number in composer.json");
      return;
    }

    $version = $data['version'];
    $name = basename( $dir );

    $combined = "{$name}-{$version}";

    $this->comment( "The zip will be named $combined.zip" );
    exec( "ssh {$username}@satis.driv.digital 'stat /var/www/drivdigital/satis/master/public/zips/{$combined}.zip 2>&1'", $output );
    if ( count( $output ) > 2 ) {
      $action = $this->choice( "The zip already exists. What  would you like to do?", [
        'a' => 'Abort',
        'r' => 'Replace',
        'd' => 'Delete',
      ] );

      if ( 'a' == $action ) {
        return;
      }
      exec( "ssh {$username}@satis.driv.digital 'sudo rm /var/www/drivdigital/satis/master/public/zips/{$combined}.zip'" );
      if ( 'd' == $action ) {
        return;
      }
    }

    if ( ! isset( $data['homepage'] ) || ! $data['homepage'] ) {
      $data['homepage'] = $this->getPluginField('plugin uri');
    }
    if ( ! isset( $data['homepage'] ) || ! $data['homepage'] ) {
      $this->comment( "The homepage is missing from composer.json" );
      $data['homepage'] = $this->ask( 'Please enter a homepage of the plugin/theme.' );
      $updated = true;
    }
    if ( ! isset( $data['description'] ) || ! $data['description'] ) {
      $description = $this->getPluginField('description');
      $pos = strpos($description, '|');
      if ($pos > 0) {
        $description = substr($description, 0, $pos);
      }
      $data['description'] = trim($description);
    }
    if ( ! isset( $data['description'] ) || ! $data['description'] ) {
      $this->comment( "The description is missing from composer.json" );
      $data['description'] = $this->ask( 'Please enter a description of the plugin/theme.' );
      $updated = true;
    }
    if ( ! isset( $data['type'] )|| ! $data['type'] ) {
      $this->comment( "The type is missing from composer.json" );
      $data['type'] = $this->choice( 'Please select a type', [
        'wordpress-plugin' => 'WordPress Plugin',
        'wordpress-theme' => 'WordPress theme',
        'project' => 'Project',
      ] );
      $updated = true;
    }

    if ( $updated && $this->confirm( 'Do you want to update the local composer.json?' ) )  {
      $json = json_encode( $data, JSON_PRETTY_PRINT );
      file_put_contents( "$dir/composer.json", $json );
    }

    $this->comment( "Temporarily copying everything to  /tmp/$combined/ for preprocessing" );

    if ( file_exists( "/tmp/$combined" ) ) {
      `rm -rf /tmp/$combined/`;
    }

    $data['dist'] = [
      'url' => 'https://satis.driv.digital/zips/',
      'type' => 'artifact',
    ];

    `cp -r $dir /tmp/$combined && rm -rf /tmp/$combined/.git /tmp/$combined/node_modules`;

    $json = json_encode( $data, JSON_PRETTY_PRINT );

    $this->comment( "Using this for the composer.json:" );
    $this->info( $json );

    if ( $this->confirm( "Does this look ok?", true ) ) {
      file_put_contents( "/tmp/$combined/composer.json", $json );

      `cd /tmp && zip -r /tmp/{$combined}.zip $combined`;
      $this->comment( "Zipped to /tmp/{$combined}.zip" );
      $this->comment( "Uploading to satis" );
      $this->info( `scp /tmp/{$combined}.zip {$username}@satis.driv.digital:/var/www/drivdigital/satis/master/public/zips` );
      # echo "The name is $combined";

      $this->comment( "Cleaning up temporary files." );
      if ( file_exists( "/tmp/{$combined}.zip" ) ) {
        `rm /tmp/$combined.zip`;
      }
    } else {
      $this->comment( "Ok, next time maybe. Cleaning up temporary files." );
    }
    if ( file_exists( "/tmp/$combined" ) ) {
      `rm -rf /tmp/$combined/`;
    }
  }
}