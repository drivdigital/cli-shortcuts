<?php

namespace App\Scripts\AppJson;

use CLI\Shortcuts\Script;

class AppJson extends \CLI\Shortcuts\Script {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'appjson';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Create a new app.json';

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $file = $this->get_project_dir() . '/app.json';
    $this->info( $file );
    if ( ! file_exists( $file ) ) {
      touch( $file );
    }
    if ( ! file_exists( $file ) ) {
      $this->error( 'Could not create app.json' );
      return;
    }
    $appjson = new \CLI\File\AppJson( $file );
    $appjson->fillBlanks( $this );
    $appjson->save();
  }
}