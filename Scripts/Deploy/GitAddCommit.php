<?php

namespace App\Scripts\Deploy;

use CLI\Shortcuts\Script;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GitAddCommit extends \CLI\Shortcuts\Script {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'add-commit {message?}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Wrapper for "git add" and "git commit"';

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $message = $this->argument( 'message' );
    if ( ! $message ) {
      $this->info( "Remember to include the jira-issue key (eg.: DTF-1)." );
      $message = $this->ask( "Describe your commit." );
    }

    // Pass the message on to argv so that Deploy\GitCommit will be able to read it.
    $GLOBALS['argv'] = [ '', '', '-m', $message ];

    exec( 'git add -A' );
    self::call( 'commit' );
  }
}