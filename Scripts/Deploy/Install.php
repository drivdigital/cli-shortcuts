<?php

namespace App\Scripts\Deploy;

use CLI\Shortcuts\Script;

class Install extends \CLI\Shortcuts\Script {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'install {project?}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Use app.json to deploy the project';

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    if ( ! $this->config_check() ) {
      return;
    }
    // Step 1 - Clone
    $config  = $this->get_config();
    $project = $this->get_var( 'project', 'What project do you want to install?' );
    $source  = $this->get_var( 'source', 'Which source do you want to get the project from?', ['bitbucket.org', 'github.com'] );
    $parts = explode( '/', $project );
    $target = array_pop( $parts );
    $command = $this->git_clone( $project, $source, $target );
    $this->comment( $command );
    if ( ! $this->confirm( 'Do you want to run this command?' ) ) {
      $this->info( 'Goodbye!' );
      return;
    }
    $result = exec( $command );

    // Step 2 - Ready the instructions from app.json
    // Step 2 - Clone the database
    // Step 2 - Composer install
    // Step 2 - Copy files
    // Step 2 - Etc..

  }

  public function git_clone( $project, $source, $target = '' ) {
    $target = preg_replace( '/\.[a-z]{1,3}$/', '.x', $target );
    return trim( sprintf( 'git clone git@%s:%s.git %s', $source, $project, $target) );
  }

  public function validate_app( $app ) {
  }

  public function fillBlanks( $app ) {
    if ( ! isset( $app ) ) {

    }
  }

  /**
   * Read JSON
   * @param  string $file
   * @return string|null
   */
  public function read_json( $file ) {
    $content = file_get_contents( $file );
    return json_decode( $content, 1 );
  }

  /**
   * Init app.json
   * @return [type] [description]
   */
  public function init_app_json() {

  }
}