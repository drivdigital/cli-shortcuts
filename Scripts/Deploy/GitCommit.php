<?php

namespace App\Scripts\Deploy;

use CLI\Shortcuts\Script;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GitCommit extends \CLI\Shortcuts\Script {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'commit';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Wrapper for "git commit"';

  /**
   * Build a custom args array
   *
   * @var array
   */
  public $args = [];

  /**
   * Run
   *
   * Use the custom ArgvInput to alter the tokens before running the command (skip argument parsing)
   * @param  InputInterface  $input
   * @param  OutputInterface $output
   * @return mixed
   */
  public function run( InputInterface $input, OutputInterface $output ) {
    if ( 'App\Console\Input\ArgvInput' == get_class( $input ) ) {
      $input->_setTokens(['commit']);
    }
    return parent::run( $input, $output );
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    if ( ! $this->config_check() ) {
      return;
    }
    $config = $this->get_config();
    $argv = array_splice( $GLOBALS['argv'], 2 );
    $message = $this->take_out_message( $argv );
    $this->args = [
      'argv' => $argv,
      'message' => $message,
    ];
    $this->inject_issue_number();

    // There is a message but checks fail
    if ( $message && ! $this->run_checks() ) {
      return;
    }

    $this->update_message();

    $command = 'git commit ';
    foreach ($this->args['argv'] as $arg) {
      if ( '-' == substr( $arg, 0, 1 ) ) {
        $command .= "$arg ";
      } else {
        $command .= escapeshellarg( $arg ) .' ';
      }
    }
    passthru( $command );
  }

  /**
   * Get Message
   * @param  array $argv arguments passed to the command
   * @return string
   */
  public function take_out_message( &$argv ) {
    $return_arg = null;
    foreach ( $argv as $key => $arg ) {
      // The option value is part of the option arg
      if ( preg_match( '/^\-m\S/', $arg ) ) {
        unset( $argv[ $key ] );
        // Return only the value
        return substr( $arg, 2 );
      }
      // We found the option flag, return the next arg
      if ( '-m' == $arg ) {
        unset( $argv[ $key ] );
        $return_arg = true;
        continue;
      }
      // Do not return an option flag
      if ( '-' == substr( $arg, 0, 1 ) ) {
        $return_arg = false;
      }
      // The previous arg was "-m" this must be the value/message
      if ( $return_arg ) {
        unset( $argv[ $key ] );
        return $arg;
      }
    }
  }

  /**
   * Get Message
   * @param  array $argv arguments passed to the command
   * @return string
   */
  public function update_message() {
    if ( null === $this->args['message'] ) {
      return;
    }
    $argv = &$this->args['argv'];
    $argv[] = '-m';
    $argv[] = $this->args['message'];
  }

  /**
   * Inject issue number
   */
  public function inject_issue_number() {
    if ( null === $this->args['message'] ) {
      return;
    }
    $branch = trim( shell_exec( 'git rev-parse --abbrev-ref HEAD' ) );
    $collection = [
      $this->args['message'],
      "$branch ",
    ];
    $issue = false;
    $reg = '/^([A-Z]+\d?\-\d+)\s/';
    foreach ( $collection as $index => $record ) {
      if ( preg_match( $reg, $record, $matches ) ) {
        $issue = trim( $matches[1] );
        break;
      }
    }
    if ( ! $issue ) {
      $valid = false;
      while ( ! $valid ) {
        $issue = $this->ask( 'Which issue number is this commit for? (leave empty to skip)', false );
        if ( $issue ) {
          $valid = preg_match( $reg, "$issue ");
        } else {
          $valid = true;
        }
        if ( ! $valid ) {
          $this->warn( 'ERROR: Required format: ABC-###' );
        }
      }
    }
    if ( $issue  && $index ) {
      $this->args['message'] = "$issue {$this->args['message']}";
    }
  }

  /**
   * Run checks
   * @return boolean
   */
  public function run_checks( ) {
    // Get global commit checks
    $checks_path = $this->get_path( 'checks/commit/' );
    $files = glob( $checks_path.'/*.php' );
    $checks_passed = true;
    // Run checks
    foreach ( $files as $file ) {
      if ( $this->run_check( $file ) ) {
        continue;
      }
      $this->info( "Failed $file !" );
      $checks_passed = false;
    }
    return $checks_passed;
  }

  /**
   * Run Check
   * @param  string  $file
   * @param  array
   * @return boolean
   */
  public function run_check( $file ) {
    // Expose args to the $file
    extract( $this->args );
    // Expose the command to allow the checks to make changes
    $command = $this;
    return require $file;
  }
}