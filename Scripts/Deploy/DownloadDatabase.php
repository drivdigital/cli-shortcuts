<?php

namespace App\Scripts\Deploy;

use CLI\Shortcuts\Script;

class DownloadDatabase extends \CLI\Shortcuts\Script {
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'ddb {instance?}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Download a database';

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $content = file_get_contents( dirname( __DIR__ ).'/app.json' );
    $data = json_decode( $content, 1 );

    $instance = reset( $data['instances'] );
    $server   = reset( $instance['servers'] );
    $database = reset( $server['databases'] );
    $whoami   = trim( `whoami` );
    $username = readline( "Username? ({$whoami})" );

    if ( ! $username ) {
      $username = $whoami;
    }

    $local = readline( "Local database name? ({$database['name']})" );
    if ( ! $local ) {
      $local = $database['name'];
    }

    $command = "ssh {$username}@{$server['host']} \"sudo mysqldump {$database['name']} | gzip\" | gunzip | sudo mysql $local";

    echo "About to run this command:\n";
    echo "$command\n\n";
    $continue = readline( "Continue? [Y/n]" );

    if ( $continue && strtolower( $continue[0] ) === 'n' ) {
      die;
    }

    passthru( $command );
  }
}