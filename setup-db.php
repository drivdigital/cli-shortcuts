<?php
require_once __DIR__ .'/load.php';

if ( ! $vagrant_config ) {
  die( "echo Could not detect a vagrant config file\n" );
}

$version = 'test';
if ( isset( $argv[1] ) ) {
  $version = $argv[1];
}

$var = @$vagrant_config[ 'db_name_'. $version ];
if ( ! $var ) {
  $var = readline( "Configure datbase name for {$version}: " );
  if ( $var ) {
    $vagrant_config[ 'db_name_'. $version ] = $var;
    save_vagrant_config();
  }
}

$var = @$vagrant_config[ 'db_username_'. $version ];
if ( ! $var ) {
  $var = readline( "Configure database username for {$version}: " );
  if ( $var ) {
    $vagrant_config[ 'db_username_'. $version ] = $var;
    save_vagrant_config();
  }
}
