<?php
error_reporting(-1);
ini_set( 'display_errors', 1 );

$files = [
  "wp/wp-load.php",
  "public/wp/wp-load.php",
];

foreach ( $files as $file ) {
  if (file_exists($file)) {
    break;
  }
}
if (! file_exists($file)) {
  die("Could not locate '$file'\n");
}
require_once $file;

$user_info = array(
  "user_pass"     => "admin",
  "user_login"    => "admin",
  "user_nicename" => "Admin",
  "user_email"    => "developers+admin@drivdigital.no",
  "display_name"  => "Admin",
  "first_name"    => "Admin",
  "last_name"     => "Admin",
);
$user = get_user_by( 'login', $user_info['user_login'] );

if ( $user ) {
  $user->user_pass = $user_info['user_pass'];
  $user->set_role('administrator');
  wp_update_user( $user );
  echo "Updated the password for \"admin\"";
} else {
  $user_id = wp_insert_user( $user_info );
  if ( is_wp_error( $user_id ) ) {
    echo "Error:\n";
    die( $user_id->get_error_message() );
  } else {
    echo "Successfully created user with id: {$user_id}\n";
    $user = new WP_User( $user_id );
    $user->set_role('administrator');
  }
}
