<?php

global $argv;
global $config;

require_once dirname( dirname( __DIR__ ) ).'/load.php';

$jql = "status=\"in%20progress\"%20AND%20assignee=\"{$config['jira']['username']}\"";
$url = "https://{$config['jira']['username']}:{$config['jira']['password']}@jira.driv.digital/rest/api/latest/search?jql=$jql";
// var_dump( $url );die;
$json = file_get_contents( $url );
$data = json_decode( $json, 1 );

$black     =  "\033[30m";
$blue      =  "\033[34m";
$green     =  "\033[32m";
$cyan      =  "\033[36m";
$red       =  "\033[31m";
$purple    =  "\033[35m";
$brown     =  "\033[33m";
$lightgray =  "\033[37m";
$reset     =  "\033[0m";
// $darkgray =  '1;30';
// $lightblue =  '1;34';
// $lightgreen =  '1;32';
// $lightcyan =  '1;36';
// $lightred =  '1;31';
// $lightpurple =  '1;35';
// $yellow =  '1;33';
// $white =  '1;37';
// var_dump( $data );

foreach ($data['issues'] as $key => $issue) {
  echo "$purple{$issue['key']}:$brown({$issue['fields']['status']['name']})$lightgray {$issue['fields']['summary']}$reset\n";
}
