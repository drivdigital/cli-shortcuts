<?php

global $argv;
global $config;

require_once dirname( dirname( __DIR__ ) ).'/load.php';

$issues = array_slice( $argv, 1 );
if ( empty( $issues ) ){
  die( 'provide some issue numbers' );
}
foreach ( $issues as $issue ) {
  $issue = strtoupper( $issue );
  if ( ! preg_match( '/^[A-Z]+\-\d+$/', $issue ) ) {
    die( "Please provide an issue number\n" );
  }
}

foreach ( $issues as $i => $issue ) {
  $issue = strtoupper( $issue );
  if ( 0 != $i ) {
    echo "\n-------------------------------------------------\n\n";
  }
  $json = file_get_contents( "https://{$config['jira']['username']}:{$config['jira']['password']}@jira.driv.digital/rest/api/latest/issue/{$issue}" );
  $data = json_decode( $json, 1 );

  $black     =  "\033[30m";
  $blue      =  "\033[34m";
  $green     =  "\033[32m";
  $cyan      =  "\033[36m";
  $red       =  "\033[31m";
  $purple    =  "\033[35m";
  $brown     =  "\033[33m";
  $lightgray =  "\033[37m";
  $reset     =  "\033[0m";
  // $darkgray =  '1;30';
  // $lightblue =  '1;34';
  // $lightgreen =  '1;32';
  // $lightcyan =  '1;36';
  // $lightred =  '1;31';
  // $lightpurple =  '1;35';
  // $yellow =  '1;33';
  // $white =  '1;37';

  echo "{$cyan}{$issue} {$green}Dear {$data['fields']['assignee']['displayName']}, please:{$reset}\n";
  echo "{$red}{$data['fields']['status']['name']}{$reset} - ";
  echo "{$brown}{$data['fields']['summary']}{$reset}\n";
  if ( $data['fields']['description'] )
    echo "{$lightgray}{$data['fields']['description']}{$reset}\n";
  echo "Reporter: {$data['fields']['reporter']['displayName']}\n";
  // explain MID-201 MID-733 MID-773 MID-774 MID-781 MID-782 MID-784 MID-785 MID-787 MID-788 MID-789 MID-791
}
