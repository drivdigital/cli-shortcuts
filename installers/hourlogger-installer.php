<?php

$repo     = 'git@bitbucket.org:drivdigital/timetracker.git';
$base_dir = dirname( __DIR__ );
$path     = $base_dir .'/vendor/drivdigital/hourlogger';

shell_exec( 'mkdir -p '. dirname( $path ) );

if ( ! file_exists( $path ) ) {
  exec( "git clone $repo '$path'", $result, $return );

  if ( 0 != $return ) {
    die( "Could not clone hourlogger/z\n" );
  }
}
if ( ! file_exists( $path.'/hours' ) ) {
	mkdir( $path.'/hours' );
}