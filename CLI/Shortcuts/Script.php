<?php

namespace CLI\Shortcuts;
use Illuminate\Console\Command;

class Script extends Command {

  /**
   * Get Path
   * @param  string $path
   * @return string
   */
  public function get_path( $path ) {
    return realpath( base_path( '../'.$path ) );
  }
  /**
   * Get config
   * @return array
   */
  public function get_config() {
    $filepath = base_path( '../config.json' );
    if ( ! file_exists( $filepath ) ) {
      $config = [];
    } else {
      $content = file_get_contents( $filepath );
      $config = json_decode( $content, 1 );
      if ( ! $config ) {
        $this->error( 'JSON-error in the config.json file' );
        return [];
      }
    }
    return $config;
  }

  /**
   * Save config
   * @param  array   $config
   * @return boolean
   */
  public function save_config( $config ) {
    $filepath = base_path( '../config.json' );
    $content = json_encode( $config, JSON_PRETTY_PRINT );
    if ( ! $content ) {
      return false;
    }
    return file_put_contents( $filepath, $content );
  }

  /**
   * Config check
   * @return [type] [description]
   */
  public function config_check() {
    $config = $this->get_config();
    if ( empty( $config ) ) {
      return false;
    }

    if ( ! isset( $config['satis.driv.digital'] ) || ! isset( $config['satis.driv.digital']['username'] ) ) {
      $config['satis.driv.digital']['username'] = $this->ask( 'What\'s your username for satis.driv.digital? (you can change this later in cli-shortcuts/config.json)' );
      if ( ! preg_match( '/^[A-Za-z\-]+$/', $config['satis.driv.digital']['username'] ) ) {
        $this->error( 'Invalid username!' );
        return false;
      }
      $this->save_config( $config );
    }
    return true;
  }

  /**
   * Get project dir
   * Warning! recursive function
   * @param  string         $cwd (Optional) Web project root directory
   * @return string|boolean      Working dir on success. False on failure
   */
  public function get_project_dir( $cwd = '' ) {
    if ( ! $cwd ) {
      $cwd = getcwd();
    }
    $parts = explode( '/', $cwd );
    $parts = array_filter( $parts );
    if ( count( $parts ) < 2 ) {
      return false;;
    }

    $files = scandir( $cwd );
    if ( in_array( 'public', $files ) ) {
      return $cwd;
    }

    if ( in_array( 'index.php', $files ) && ! in_array('public', $parts ) ) {
      $this->error( 'The index.php should be inside a public folder!' );
      return $cwd;
    }

    return $this->get_project_dir( dirname( $cwd ) );
  }

  /**
   * Get var
   */
  public function get_var( $config, $question, $alternatives = [] ) {
    if ( $this->hasArgument( $config ) ) {
      $var = $this->argument( $config );
      if ( $var ) {
        return $var;
      }
    }
    if ( ! empty( $alternatives ) ) {
      return $this->choice( $question, $alternatives, '0' );
    }
    return $this->ask( $question );
  }

  /**
   * Get base dir
   */
  public function get_base_dir( $cwd = '' ) {
    $this->info('The `get_base_dir` method is redundant!');
    $dir = $this->get_project_dir();
    if ( 'public' == basename( $dir ) ) {
      return dirname( $dir );
    }
    return $dir;
  }
}