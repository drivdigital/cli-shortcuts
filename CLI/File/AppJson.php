<?php

namespace CLI\File;

use CLI\Support\Query;

/**
 *  App.json
 */
class AppJson// extends File
{
    protected $data;
    protected $path;


    /**
     * Construct
     *
     * @param string $path Path to the app.json file.
     */
    function __construct($path)
    {
        if (! file_exists($path)) {
            throw new \Exception("Error opening app.json, $path!");
        }
        $this->path = $path;
        $content = file_get_contents($path);
        $this->data = json_decode($content, 1);
    }

    /**
     * Save
     *
     * @return void
     */
    public function save()
    {
        $content = json_encode($this->data, JSON_PRETTY_PRINT);
        file_put_contents($this->path, $content);
    }

    /**
     * Fill blanks
     *
     * @param object $command Command interface.
     *
     * @return void
     */
    public function fillBlanks( $command )
    {
        $queries = [];
        $queries[] = ( new Query('name', 'Project name'));
        $queries[] = ( new Query('type', 'Type'))
            ->setResolver('choice')
            ->setChoices([ 'laravel', 'wordpress', 'magento-1.x', 'magento-2.x', 'other' ]);
        if (! isset($this->data['instances']) || ! is_array($this->data['instances'])) {
            $this->data['instances'] = [];
        }
        $this->data = $this->resolveQueries($queries, $command, $this->data);
        if (! array_search('test', array_column($this->data['instances'], 'id'))) {
            $this->instanceWizard($command, 'test');
        }
        if (! array_search('live', array_column($this->data['instances'], 'id'))) {
            $this->instanceWizard($command, 'live');
        }
    }

    /**
     * Get instances
     *
     * @return array
     */
    public function getInstances()
    {
        return $this->data['instances'];
    }

    /**
     * Instance Wizard
     *
     * @param object $command Command interface.
     * @param string $id    Instance. eg 'live', 'local', 'test'.
     *
     * @return void
     */
    public function instanceWizard( $command, $id )
    {
        if (! $command->confirm("Would you like to create a $id instance?")) {
            $command->info('Ok.');
            return;
        }
        $queries[] = ( new Query('environment', 'Environment?'))
            ->setResolver('choice')
            ->setChoices([ 'production', 'development', 'local' ]);
        $queries[] = ( new Query('servers.0.username', 'Username?'))
            ->setRequired(false);
        $queries[] = ( new Query('servers.0.host', 'Hostname?'));
        $queries[] = ( new Query('servers.0.dir', 'Project directory? (NB! not the public dir)'));
        $instance = [
            'id' => $id,
        ];
        $instance = $this->resolveQueries($queries, $command, $instance);
        $this->data['instances'][] = $instance;
    }

    /**
     * Resolve queries
     *
     * @param array  $queries Array of questions.
     * @param object $command Command interface.
     * @param array  $array   Array of answers.
     *
     * @return array          Array of answers.
     */
    public function resolveQueries( $queries, $command, $array )
    {
        foreach ( $queries as $query ) {
            $query->resolve($command, $array);
        }
        return $array;
    }

    /**
     * Create Instance
     *
     * @param array $instance_data Instance data.
     *
     * @return void
     */
    public function createInstance( $instance_data )
    {
        $sub_queries = [];
        $fields = [
            'id',
            'environment',
            'server' => [
            'username',
                'host',
                'dir',
            ]
        ];


        //   'environment' =>
        // string(10) "production"
        // 'domain' =>
        // string(8) "serie.no"
        // 'aliases' =>
        // array(1) {
        //   [0] =>
        //   string(10) "univers.no"
        // }
        // 'server' =>
        // array(3) {
        //   'username' =>
        //   string(11) "flippn_3124"
        //   'host' =>
        //   string(20) "rask22.raskesider.no"
        //   'dir' =>
        //   bool(false)
        // }
        // 'databases' =>
        // array(1) {
        //   'laravel' =>
        //   array(3) {
        //     'type' =>
        //     string(5) "mysql"
        //     'exclude_data' =>
        //     array(0) {
        //       ...
        //     }
        //     'credentials' =>
        //     array(3) {
        //       ...
        //     }
        //   }
    }
}