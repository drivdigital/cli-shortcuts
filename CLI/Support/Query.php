<?php

namespace CLI\Support;

/**
 *  Query
 */
class Query
{
  public $slug      = '';
  public $question  = '';
  public $default   = null;
  public $resolver  = 'ask';
  public $overwrite = false;
  public $required  = true;
  public $choices   = [];

  function __construct( $slug, $question ) {
    $this->slug     = $slug;
    $this->question = $question;
  }

  function __call( $key, $args ) {
    if ( ! preg_match( '/^set([A-Za-z]+)$/', $key, $matches ) ) {
      throw new \Exception( "No such method, $key!" );
    }

    if ( count( $args ) != 1 ) {
      throw new Exception( "Setters require exactly 1 argument!" );
    }
    $key = strtolower( preg_replace('/(?<!^)[A-Z]/', '_$0', $matches[1] ) );
    if ( property_exists( $this, $key ) ) {
      $this->{$key} = reset( $args );
    }
    return $this;
  }

// resolve ('name.test.test', 'something', ['instances'=>[]])
// $array['name']['test']['test'] = 'something';
// $array['instances'] = [];

  /**
   * Resolve
   * @param  Command $command
   * @param  array   &$array
   * @return string
   */
  function resolve( $command, &$array ) {
    $parts = explode( '.', $this->slug );
    // $parts = ['name','test','test'];
    $key = array_pop( $parts );
    // $parts = ['name','test'];
    // $key = 'test'

    $containers = [
      &$array
    ];

    // Dig deeper and deeper into the array
    $depth = 0;
    while ( ! empty( $parts ) ) {
      $container = &$containers[ $depth ];
      $_key = array_shift( $parts );
      if ( ! isset( $container[ $_key ] ) ) {
        // Create the key if it doesn't exist
        $container[ $_key ] = [];
      }
      if ( ! is_array( $container[ $_key ] ) ) {
        // The path is not an array, stop here
        return;
      }
      // Keep the reference intact
      $containers[] = &$container[ $_key ];
      $depth++;
    }

    // Assign and check the last container
    $container = &$containers[ $depth ];
    if ( isset( $container[ $key ] ) && ! $this->overwrite ) {
      // echo "The path {$this->slug} has already been set. Returning\n";
      return;
    }
    $default = $this->default;
    $question = $this->question;
    if ( ! $this->required ) {
      $question .= ' (optional)';
      if ( null === $default ) {
        $default = false;
      }
    }
    // Ask what needs to be asked
    $args = [
      $question,
      $default,
    ];
    if ( 'choice' == $this->resolver ) {
      $args[1] = $this->choices;
      $args[2] = $default;
    }
    // Get the value and assign it
    $value = call_user_func_array( [ $command, $this->resolver ], $args );
    $container[ $key ] = $value;

    return $value;
  }

  /**
   * Dive
   * @param  array &$array
   * @param  array $parts
   * @return array
   */
  function dive( &$array, $parts ) {
    return $this->dive( $array[ $key ], $parts );

  }
}