<?php


$dir = $command->get_project_dir();
if ( ! $dir ) {
	return true;
}
// Check the json file
if ( ! is_dir( $dir .'/public' ) ) {
  $command->warn( 'Warning! This project should use a public folder!' );
}

return true;
