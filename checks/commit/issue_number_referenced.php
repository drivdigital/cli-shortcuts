<?php

$config = $command->get_config();

if ( empty( $config ) ) {
	return true;
}
// Ref app
if ( ! isset( $config['jira-project'] ) ) {
	return true;
}

if ( ! preg_match( '/[A-Z]+\-\d+/', $message ) ) {
  if ( ! $command->confirm( 'No issue number was referenced. Do you still want to commit?' ) ) {
    return false;
  }
}

return true;
