<?php

$dir = $command->get_project_dir();
// Check the json file
if ( ! is_file( $dir .'/app.json' ) ) {
  $command->warn( 'There is no app.json. Run `appjson` to create it!' );
}

return true;
