<?php
require_once __DIR__ .'/load.php';

$last_time = 0;
// Check for updates every 3 hours
if ( file_exists( __DIR__ .'/.update-time' ) ) {
  $last_time = filemtime( __DIR__ .'/.update-time' );
}
$elapsed_time = ( time() - $last_time ) / ( 60 * 60 );

// Touch before pulling to prevent double pull when opening another tab
touch( __DIR__ .'/.update-time' );

if ( $elapsed_time > 3 ) {
  chdir( __DIR__ );
  echo "Updating shortcuts\n";
  passthru( 'nohup git pull  > /dev/null 2>&1 &' );
  chdir( 'laravel' );
  passthru( 'nohup composer install > /dev/null 2>&1 &' );
}
