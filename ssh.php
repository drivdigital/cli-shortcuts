<?php
require_once __DIR__ .'/load.php';

if ( ! $vagrant_config ) {
  die( "echo Could not detect a vagrant config file\n" );
}

$version = 'test';
if ( isset( $argv[1] ) ) {
  $version = $argv[1];
}

if ( ! isset( $vagrant_config[ 'ssh_'. $version ] ) ) {
  die( 'echo "SSH is not configured properly in vagrant-config/config.json"' );
}

$ssh = $vagrant_config[ 'ssh_'. $version ];
echo 'ssh ' . $ssh;
