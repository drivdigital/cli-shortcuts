<?php
define( 'ROOT_PATH', __DIR__ );

require_once __DIR__ .'/load.php';

$shortcuts = [];

$shortcuts[] = require 'includes/git.php';
$shortcuts[] = require 'includes/wordpress.php';
$shortcuts[] = require 'includes/magento.php';
$shortcuts[] = require 'includes/vagrant.php';
$shortcuts[] = require 'includes/composer.php';
$shortcuts[] = require 'includes/ssh.php';
$shortcuts[] = require 'includes/z.php';
$shortcuts[] = require 'includes/hourlogger.php';
$shortcuts[] = require 'includes/jira.php';
$shortcuts[] = require 'includes/deployment.php';

$sections = [];
foreach ( $shortcuts as $section ) {
  if ( ! isset( $section['name'] ) ) {
    continue;
  }
  $section_name = strtolower( $section['name'] );
  if ( isset( $sections[ $section_name ] ) ) {
    $sections[ $section_name ]['shortcuts'] = array_merge(
      $sections[ $section_name ]['shortcuts'],
      $section['shortcuts']
    );
  } else {
    $sections[ $section_name ] = $section;
  }
}
$lines = [];
$help = [];
$help[] = 'function help() {';
foreach ( $sections as $section ) {
  if ( ! isset( $section['shortcuts'] ) || ! isset( $section['name'] ) ) {
    continue;
  }
  $name = strtoupper( $section['name'] );
  $help[] = "\techo \"\\n$name\"";
  foreach ( $section['shortcuts'] as $id => $parameters ) {
    $type = $parameters['type'];
    if ( 'alias' == $type ) {
      $command = $parameters['command'];
      $command = str_replace( "'",'"', $command );
      $command = addcslashes( $command, "'" );
      $command = str_replace( '__DIR__', __DIR__, $command );
      if ( ! isset( $parameters['echo'] ) || $parameters['echo'] ) {
        $command = 'echo "'. addcslashes( $command, '"' ) ."\" && $command";
      }
      $lines[] = "alias $id='$command'";

      $alias = $id;

      if ( isset( $parameters['alternatives'] ) ) {
        foreach ( $parameters['alternatives'] as $alternative ) {
          $lines[] = "alias $alternative=$id";
          $alias = "$alternative|$alias";
        }
      }

      $alias = str_pad( $alias, 10, ' ' );
      $help[]  = "\techo '\\t$alias • {$parameters['description']}'";
    } else if ( 'source' == $type ) {
      $path = $parameters['path'];
      $lines[] = "source '$path'";
    } else if ( 'function' == $type ) {

    } else {
      echo "???\n";
    }
  }
  $help[] = "";
}
$help[] = "\t".'echo "\nGENERAL"';
$help[] = "\t".'echo "\thelp       • Show this help\n"';
$help[] = '}';

file_put_contents( '/tmp/shortcuts',
  implode( "\n", $lines ) ."\n". implode( "\n", $help ) ."\n"
);
