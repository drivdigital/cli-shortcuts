<?php

/**
 * Load.php
 *
 * Execution order:
 *
 * php $SELF_DIR/update.php
 *                - load.php
 * php $SELF_DIR/write-shortcuts.php
 *                - load.php
 *
 * This script is loaded on every new terminal session
 */
global $config;
global $vagrant_config;
$config = false;

if ( ! file_exists( __DIR__ . '/config.json' ) ) {
  $path = __DIR__;
  echo "Warning! $path/config.json does not exist...\n";
  return;
}
$config_file = __DIR__ . '/config.json' ;
$content = file_get_contents( $config_file );
$config = json_decode( $content, 1 );

if ( file_exists( 'app/laravel-functions.php' ) ) {
  require_once( 'app/laravel-functions.php' );
  validate_config( $config );
}

$vagrant_config = false;
if ( file_exists( 'vagrant-config/config.json' ) ) {
  $vagrant_config = json_decode( file_get_contents( 'vagrant-config/config.json' ), 1 );
  if ( ! $vagrant_config ) {
    die( 'echo "Error, '. getcwd() .' /vagrant-config/config.json does not contain valid json"' );
  }
}

function save_vagrant_config() {
  global $vagrant_config;
  if ( file_exists( 'vagrant-config/config.json' ) ) {
    file_put_contents( 'vagrant-config/config.json', json_encode( $vagrant_config, JSON_PRETTY_PRINT ) );
  }
}
