# CLI Shortcuts
Some additional shortcuts to make it easier to develop using feature-branching

## Installation

Zsh:

```
cd ~ && git clone git@bitbucket.org:drivdigital/cli-shortcuts.git && echo ". ~/cli-shortcuts/shortcuts" >> ~/.zshrc && . ~/cli-shortcuts/shortcuts
```

Bash (untested):

```
cd ~ && git clone git@bitbucket.org:drivdigital/cli-shortcuts.git && echo ". ~/cli-shortcuts/shortcuts" >> ~/.bash_profile && . ~/cli-shortcuts/shortcuts
```

## GIT
| Shortcut | Description |
| -------- | ----------- |
| `gp1` | Push a branch for the first time |
| `m|master` | Checkout and pull master |
| `d|develop` | Checkout and pull develop |
| `gpr` | (git pull-request) Create a new pull request of the current branch to develop |

## PLATFORM SPECIFIC (MAGENTO/WORDPRESS/LARAVEL)
| Shortcut | Description |
| -------- | ----------- |
| `magev` | Gets the magento version|

## SSH
| Shortcut | Description |
| -------- | ----------- |
| `x` | SSH into test |
| `xgrep` | SSH into test and grep for DB details |
| `xx` | SSH into live |
| `xxdb` | SSH into live and download the database |
| `xxgrep` | SSH into live and grep for DB details |

## VAGRANT
| Shortcut | Description |
| -------- | ----------- |
| `v` | Vagrant |
| `vs` | Vagrant ssh |
| `vss` | Vagrant ssh restart apache2 |
| `vu` | Vagrant up |
| `vh` | Vagrant halt |
| `vap` | Vagrant up --provision |
| `vd` | Vagrant destroy -f |
| `vdu` | Vagrant destroy -f + vagrant up |
| `vdap` | Vagrant destroy -f + vagrant up --provision |
| `vgs` | Vagrant global-status |
| `fresh` | Start a new vagrant project |

## GENERAL
| Shortcut | Description |
| -------- | ----------- |
| `shortcuts` | Edit the shortcodes |
| `help` | Show this help |

### Your workflow might look something like this:

- Checkout the jira ticket you're starting to work with: `gco -b EXG-123`
- When you're finished with the task:`git add * && git commit -m "Updated #EXG-123 with fixes and added new feature that does this task"`
- Push your changes to the repo `gp1`
- Create a pull request with `gpr`
- Visit the link & code review the changes. Have it reviewed by peers.
- Checkout develop `d`
- SSH into the test site by using `x` and cd into public and run git pull and composer install
- Create a pull request to master when you are ready to publish `gpr`, and accept by entering "y" + enter
- Approve & merge the changes with the Master branch
- SSH into the live site by using `xx` and cd into public and run git pull and composer install
