<?php

require_once __DIR__ .'/load.php';

$args = [];
foreach ( $argv as $value ) {
  if ( '-' != $value ) {
    $args[] = $value;
  }
}

array_shift( $args );

$username = @$config['bitbucket']['username'];
$password = @$config['bitbucket']['password'];
if ( ! $username ) {
  $username = readline( 'Bitbucket username: ' );
}
if ( ! $password ) {
  $password = readline( 'Bitbucket password: ' );
}

$branch = trim( `git rev-parse --abbrev-ref HEAD` );
$target = 'develop';

if ( 'master' == $branch ) {
  die( "You are not allowed to make a pull request from master\n" );
}
if ( 'develop' == $branch  ) {
  $target = 'master';
  if ( ! in_array( '-y', $argv ) && strtolower( readline( 'Are you sure you want to develop to be pulled into master? [N|y]: ' ) ) != 'y' ) {
    die( "You selected \"N\", bye.\n" );
  }
}
if ( ! empty( $args ) ) {
  $target = $args[0];
}

if ( $target == $branch ) {
  die( "You are not allowed to make a pull request to the same branch that you are on\n" );
}

$git_url = trim( `git config --get remote.origin.url` );

if ( ! $git_url ) {
  die( "Could not identify the origin URL to create a pull request with\n" );
}

// Parse $account and $repo
if ( ! preg_match( '/[:\/](.+)\/(.+).git$/', $git_url, $matches ) ) {
  die( "Unable to parse the git origin url: $url\n" );

}
$account = $matches[1];
$repo = $matches[2];

$password = rawurlencode( $password );

$ch = curl_init();
curl_setopt( $ch, CURLOPT_URL,"https://$username:$password@bitbucket.org/api/2.0/repositories/$account/$repo/pullrequests" );
curl_setopt( $ch, CURLOPT_POST, 1 );
curl_setopt( $ch, CURLOPT_HTTPHEADER, [ 'Content-Type: application/json' ] );
curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( [
  'title' => "Merge branch $branch",
  'description' => '',
  'source' => [
    'branch' => [ 'name' => $branch ],
  ],
  'destination' => [
    'branch' => [ 'name' => $target ],
  ],
  'reviewers' => [],
  // Close the branch if the target is develop
  'close_source_branch' => ( $target == 'develop' ),
] ) );

// receive server response ...
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
$result = curl_exec( $ch );
curl_close( $ch );
$data = json_decode( $result, 1 );

$link = @$data['links']['html'];
if ( ! empty( $link ) ) {
  echo "New pull request created: {$link['href']}\n";
} else {
  echo "Warning: An unhandled error occurred!\n";
  echo "Your password might be wrong, check the config.json\n";
  var_dump( $result );
  die;
}
