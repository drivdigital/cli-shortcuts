<?php

$section = [
  'name' => 'vagrant',
  'shortcuts' => [
    'v' => [
      'type' => 'alias',
      'command' => 'vagrant',
      'description' => 'Vagrant',
    ],
    'v' => [
      'type' => 'alias',
      'command' => "vagrant",
      'description' => 'Vagrant',
    ],
    'vs' => [
      'type' => 'alias',
      'command' => "vagrant ssh",
      'description' => 'Vagrant ssh',
    ],
    'vss' => [
      'type' => 'alias',
      'command' => "vagrant ssh -c 'sudo service apache2 restart'",
      'description' => 'Vagrant ssh restart apache2',
    ],
    'vh' => [
      'type' => 'alias',
      'command' => "vagrant halt",
      'description' => 'Vagrant up',
    ],
    'vu' => [
      'type' => 'alias',
      'command' => "vagrant up",
      'description' => 'Vagrant halt',
    ],
    'vap' => [
      'type' => 'alias',
      'command' => "vagrant up --provision",
      'description' => 'Vagrant up --provision',
    ],
    'vd' => [
      'type' => 'alias',
      'command' => "vagrant destroy -f",
      'description' => 'Vagrant destroy -f',
    ],
    'vdu' => [
      'type' => 'alias',
      'command' => "vagrant destroy -f && vagrant up",
      'description' => 'Vagrant destroy -f + vagrant up',
    ],
    'vdap' => [
      'type' => 'alias',
      'command' => "vagrant destroy -f && vagrant up --provision",
      'description' => 'Vagrant destroy -f + vagrant up --provision',
    ],
    'vgs' => [
      'type' => 'alias',
      'command' => "vagrant global-status",
      'description' => 'Vagrant global-status',
    ],
    'fresh' => [
      'type' => 'alias',
      'command' => "git clone git@github.com:drivdigital/driv-vagrant.git",
      'description' => 'Start a new vagrant project',
    ],
  ],
];

if ( file_exists( ROOT_PATH .'/vendor/drivdigital/driv-vagrant-installer' ) ) {
  $section['shortcuts']['dvi'] = [
    'type' => 'alias',
    'command' => ROOT_PATH .'/vendor/drivdigital/driv-vagrant-installer/dvi',
    'description' => 'Install an existing vagrant project',
    'echo' => false,
  ];
}

return $section;