<?php

return [
  'name' => 'wordpress',
  'shortcuts' => [
    'wpadmin' => [
      'type' => 'alias',
      'command' => 'php __DIR__/old_scripts/wordpress/insert-user.php',
      'description' => 'Insert a WordPress admin with the username/password "admin:admin"',
      'echo' => false,
    ],
    'wpht' => [
      'type'        => 'alias',
      'command'     => 'php __DIR__/laravel/artisan htaccess',
      'description' => 'Create a htaccess file for WordPress',
      'echo'        => false,
    ],
  ],
];
