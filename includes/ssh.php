<?php

return [
  'name' => 'SSH',
  'shortcuts' => [
    'x' => [
      'type' => 'alias',
      'command' => 'php __DIR__/setup-ssh.php && `php __DIR__/ssh.php`',
      'description' => 'SSH into test',
      'echo' => false,

    ],
    'xgrep' => [
      'type' => 'alias',
      'command' => 'php __DIR__/setup-ssh.php test && php __DIR__/ssh-grep.php test',
      'description' => 'SSH into test and grep for DB details',
      'echo' => false,

    ],
    'xdb' => [
      'type' => 'alias',
      'command' => 'php __DIR__/setup-ssh.php test && php __DIR__/setup-db.php test && php __DIR__/ssh-db.php test',
      'description' => 'SSH into test and download the database',
      'echo' => false,

    ],
    'xx' => [
      'type' => 'alias',
      'command' => 'php __DIR__/setup-ssh.php live && `php __DIR__/ssh.php live`',
      'description' => 'SSH into live',
      'echo' => false,

    ],
    'xxgrep' => [
      'type' => 'alias',
      'command' => 'php __DIR__/setup-ssh.php live && php __DIR__/ssh-grep.php live',
      'description' => 'SSH into live and grep for DB details',
      'echo' => false,

    ],
    'xxdb' => [
      'type' => 'alias',
      'command' => 'php __DIR__/setup-ssh.php live && php __DIR__/setup-db.php live && php __DIR__/ssh-db.php live',
      'description' => 'SSH into live and download the database',
      'echo' => false,

    ],
  ],
];