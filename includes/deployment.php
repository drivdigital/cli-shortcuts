<?php

return [
  'name' => 'Deployment',
  'shortcuts' => [
    'satis' => [
      'type'        => 'alias',
      'command'     => 'php __DIR__/laravel/artisan satis',
      'description' => 'Compile satis. CD into the theme/plugin you want to upload to satis. Then run this command.',
      'echo'        => false,
    ],
    'deploy' => [
      'type'        => 'alias',
      'command'     => 'php __DIR__/laravel/artisan deploy',
      'description' => 'Deploy to the server using app.json',
      'echo'        => false,
    ],
    'bump' => [
      'type'        => 'alias',
      'command'     => 'php __DIR__/laravel/artisan bump',
      'description' => 'Bump the version number',
      'echo'        => false,
    ],
    'install' => [
      'type'        => 'alias',
      'command'     => 'php __DIR__/laravel/artisan install',
      'description' => 'Install a project',
      'echo'        => false,
    ],
    'gc' => [
      'type'        => 'alias',
      'command'     => 'php __DIR__/laravel/artisan commit',
      'description' => 'Wrapper for "git commit"',
      'echo'        => false,
    ],
    'gac' => [
      'type'        => 'alias',
      'command'     => 'php __DIR__/laravel/artisan -v add-commit',
      'description' => 'Wrapper for "git add -A && git commit"',
      'echo'        => false,
    ],
    'appjson' => [
      'type'        => 'alias',
      'command'     => 'php __DIR__/laravel/artisan appjson',
      'description' => 'Create a new app.json"',
      'echo'        => false,
    ],
  ],
];