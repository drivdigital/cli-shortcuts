<?php

return [
  'name' => 'magento',
  'shortcuts' => [
    'magev' => [
      'type' => 'alias',
      'command' => 'grep "function getVersionInfo()" -A10 app/Mage.php',
      'description' => 'Gets the magento version',
    ],
  ],
];
