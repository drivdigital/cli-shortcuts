<?php
if ( ! file_exists( ROOT_PATH . '/vendor/rupa/z/z.sh' ) ) {
  return [];
}
return [
  'name' => 'z',
  'shortcuts' => [
    '_z' => [
      'type' => 'source',
      'path' => ROOT_PATH . '/vendor/rupa/z/z.sh',
    ],
    'z' => [
      'type' => 'alias',
      'command' => '_z',
      'description' => 'Jump around',
      'echo' => false,
    ],
  ],
];
