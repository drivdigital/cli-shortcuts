<?php

$section = [
  'name' => 'composer',
  'shortcuts' => [
    'c' => [
      'type' => 'alias',
      'command' => 'composer',
      'description' => 'composer',
    ],
    'cr' => [
      'type' => 'alias',
      'command' => 'composer require',
      'description' => 'composer require',
    ],
    'ci' => [
      'type' => 'alias',
      'command' => 'composer install',
      'description' => 'composer install',
    ],
    'cu' => [
      'type' => 'alias',
      'command' => 'composer update',
      'description' => 'composer update',
    ],
  ],
];

return $section;
