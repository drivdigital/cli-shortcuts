<?php

return [
  'name' => 'git',
  'shortcuts' => [
    'gp1' => [
      'type' => 'alias',
      'command' => 'git push -u origin `git rev-parse --abbrev-ref HEAD`',
      'description' => 'Push a branch for the first time',
    ],
    'gac' => [
      'type' => 'alias',
      'command' => 'git add -A && git commit -m',
      'description' => 'Add all and commit',
    ],
    'master' => [
      'type' => 'alias',
      'command' => 'git checkout master && git pull',
      'description' => 'Checkout and pull master',
      'alternatives' => ['m'],
    ],
    'develop' => [
      'type' => 'alias',
      'command' => 'git checkout develop && git pull',
      'description' => 'Checkout and pull develop',
      'alternatives' => ['d'],
    ],
    'gpr' => [
      'type' => 'alias',
      'command' => 'php __DIR__/pull-request.php',
      'description' => '(git pull-request) Create a new pull request of the current branch to develop',
      'echo' => false,
    ],
    'gso' => [
      'type' => 'alias',
      'command' => 'gr set-url --push origin',
      'description' => '(git repo set-url origin to new url) ',
    ],
  ],
];
