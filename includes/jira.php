<?php
return [
  'name' => 'Jira',
  'shortcuts' => [
    'explain' => [
      'type' => 'alias',
      'command' => 'php __DIR__/old_scripts/jira/jira.php',
      'description' => 'Get the title and description of an issue',
      'echo' => false,
    ],
    'search' => [
      'type' => 'alias',
      'command' => 'php __DIR__/old_scripts/jira/jira_search.php in-progress',
      'description' => 'Get list of issues',
      'echo' => false,
    ],
  ],
];
