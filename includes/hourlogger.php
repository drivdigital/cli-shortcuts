<?php
$path = ROOT_PATH .'/vendor/drivdigital/hourlogger/run';

if ( file_exists( $path ) ) {
  $config['hourlogger'] = $path;
}
if ( ! isset( $config["hourlogger"] ) ) {
  // @todo or /vendor/drivdigital/hourlogger
  return [];
}

return [
  'name' => 'hourlogger',
  'shortcuts' => [
    'tt' => [
      'type' => 'alias',
      'command' => "if [ `ps aux | grep '{$config['hourlogger']}' | wc -l | tr -d '[:space:]'` -gt 1 ]; then echo 'Hourlogger is already running'; else nohup '{$config['hourlogger']}' >/dev/null 2>&1 &; fi",
      'description' => 'Start the hourlogger',
      'echo' => false,
    ],
  ],
];
