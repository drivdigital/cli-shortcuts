<?php
require_once __DIR__ .'/load.php';

if ( ! $vagrant_config ) {
  die( "echo Could not detect a vagrant config file\n" );
}

$version = 'test';
if ( isset( $argv[1] ) ) {
  $version = $argv[1];
}

$var = @$vagrant_config[ 'ssh_'. $version ];
if ( ! $var ) {
  $var = readline( "Configure ssh for ssh_{$version}: " );
  if ( $var ) {
    $vagrant_config[ 'ssh_'. $version ] = $var;
    save_vagrant_config();
  }
}

echo "Connecting to $version at $var\n";
