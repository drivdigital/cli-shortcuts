<?php
require_once __DIR__ .'/load.php';

if ( ! $vagrant_config ) {
  die( "echo Could not detect a vagrant config file\n" );
}

$version = 'test';
if ( isset( $argv[1] ) ) {
  $version = $argv[1];
}

if ( ! isset( $vagrant_config[ 'ssh_'. $version ] ) ) {
  die( 'echo "SSH is not configured properly in vagrant-config/config.json"' );
}

if ( ! isset( $vagrant_config[ 'db_name_'. $version ] ) ) {
  die( 'echo "DB name is not configured properly in vagrant-config/config.json"' );
}
$db_name = $vagrant_config[ 'db_name_'. $version ];

if ( ! isset( $vagrant_config[ 'db_name_'. $version ] ) ) {
  die( 'echo "DB name is not configured properly in vagrant-config/config.json"' );
}
$db_username = $vagrant_config[ 'db_username_'. $version ];

$dir = getcwd();
$name = basename( $dir );
$name = preg_replace( '/\.dev$/', '', $name );
$target_path = dirname( $dir ) .'/config/';
$path = $target_path . $name .'.sql';
$format = "ssh %s 'mysqldump --single-transaction -p -u %s %s | gzip' | gunzip > '%s'";

if ( ! file_exists( $target_path ) ) {
  $target_path = dirname( $dir ) .'/databases/';
  $path = $target_path . $name .'.sql.gz';
  $format = "ssh %s 'mysqldump -p -u %s %s | gzip' > '%s'";
}
if ( ! file_exists( $target_path ) ) {
  die( "Path, $target_path, doesn't exist" );
}

$ssh = $vagrant_config[ 'ssh_'. $version ];
// <<<<<<< Updated upstream
$command = sprintf( $format, $ssh, $db_username, $db_name, $path );
// =======
// $command = "ssh $ssh 'mysqldump --single-transaction -p -u $db_username $db_name | gzip' | gunzip > '$path'";
// >>>>>>> Stashed changes
echo "$command\n";
echo shell_exec( $command );
