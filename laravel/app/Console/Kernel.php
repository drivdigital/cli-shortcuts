<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        // Load the base model
        require base_path( '../CLI/Support/Query.php' );
        require base_path( '../CLI/File/AppJson.php' );
        require base_path( '../CLI/Shortcuts/Script.php' );

        // Magic time
        $cwd = getcwd();
        // Cd to the cli-shortcuts root dir
        chdir( base_path( '..' ) );
        // Load all the class files because they are outside of where the autoloader can get them
        $classes = glob( 'Scripts/**/*.php' );
        foreach ( $classes as $filepath ) {
            require_once $filepath;
        }
        // The `load` method uses the path to determine the namespace
        // In this case we use "Scripts" as our namespace, and that then has to also be the directory name.
        $this->load( 'Scripts' );

        // Redundant / not needed
        // require base_path( '../app/script-loader.php' );
        chdir( $cwd );
    }
}
