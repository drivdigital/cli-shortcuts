<?php

namespace App\Console\Input;

use Symfony\Component\Console\Input\ArgvInput as SymfonyArgvInput;

class ArgvInput extends SymfonyArgvInput {

    /**
     * Set Tokens
     *
     * ArgvInput->setTokens is protected and normally inaccesible.
     * This is a simple wrapper to expose this method.
     *
     * @param array $var
     */
    public function _setTokens( $tokens ) {
      $this->setTokens( $tokens );
    }
}